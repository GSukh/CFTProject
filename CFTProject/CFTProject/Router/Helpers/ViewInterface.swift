//
//  ViewInterface.swift
//  CFTProject
//
//  Created by Sukhorukov Grigory on 18.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

protocol ViewInterface: class {
	func asViewController() -> UIViewController?
}
