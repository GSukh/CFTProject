//
//  AlertHelpers.swift
//  CFTProject
//
//  Created by Sukhorukov Grigory on 18.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

struct AlertText {
	var title: String?
	var message: String?
	
	init(title: String?, message: String?) {
		self.title = title
		self.message = message
	}
}

struct AlertAction {
	
	enum Style {
		case defauld
		case cancel
		case delete
		
		var alertStyle: UIAlertActionStyle {
			switch self {
			case .defauld: return UIAlertActionStyle.default
			case .cancel: return UIAlertActionStyle.cancel
			case .delete: return UIAlertActionStyle.destructive
			}
		}
	}
	
	var title: String
	var action: () -> Void
	var style: Style
	
	init(title: String, style: Style, action: @escaping () -> Void ) {
		self.title = title
		self.action = action
		self.style = style
	}
}
