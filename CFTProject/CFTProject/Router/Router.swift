//
//  Router.swift
//  CFTProject
//
//  Created by Sukhorukov Grigory on 18.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

enum Routing {
	case choosePhoto
	case actionSheet(AlertText, [AlertAction], () -> Void)
}

typealias Module = (view: ViewInterface, observer: RouterObserver?)

class Router: NSObject {
	
	static var shared = Router()
	fileprivate weak var observer: RouterObserver?
	
	fileprivate lazy var imagePicker = UIImagePickerController()

	func rout(_ routing: Routing, on module: Module) {
		switch routing {
		case .choosePhoto:
			guard let vc = module.view.asViewController() else { return }
			self.observer = module.observer
			self.choosePhoto(on: vc)
		case .actionSheet(let alertText, let actions, let cancel):
			guard let vc = module.view.asViewController() else { return }
			self.showAlert(on: vc, text: alertText, actions: actions, cancel: cancel)
		default: break
		}
		
	}
}

fileprivate extension Router {
	
	func showAlert(on viewController: UIViewController, text: AlertText, actions: [AlertAction], cancel: (() -> Void)?) {
		let alertController = UIAlertController(title: text.title, message: text.message, preferredStyle: .actionSheet)
		for action in actions {
			let alertAction = UIAlertAction(title: action.title, style: action.style.alertStyle, handler: { (_) in
				action.action()
			})
			alertController.addAction(alertAction)
		}
		
		if let cancel = cancel {
			let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (_) in
				cancel()
			})
			alertController.addAction(cancelAction)
		}
		
		viewController.present(alertController, animated: true, completion: nil)
	}
}

fileprivate extension Router {
	
	func choosePhoto(on viewController: UIViewController) {
		let alertText = AlertText.init(title: "Image selection", message: "Choose your option")
		let cameraAction = AlertAction.init(title: "Camera", style: .defauld) {
			self.choosePhotoFromCamera(on: viewController)
		}
		let galleryAction = AlertAction.init(title: "Gallery", style: .defauld) {
			self.choosePhotoFromGallery(on: viewController)
		}
		let mockUpAction = AlertAction.init(title: "New York", style: .defauld) {
			if let image = UIImage.init(named: "NewYork.jpg") {
				if let observer = self.observer {
					observer.selectedImage(image)
				}
			}
			self.observer = nil
		}
		showAlert(on: viewController, text: alertText, actions: [cameraAction, galleryAction, mockUpAction], cancel: {} )
	}
	
	func choosePhotoFromCamera(on viewController: UIViewController) {
		if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
			self.imagePicker.delegate = self
			self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
			imagePicker.allowsEditing = true
			viewController.present(self.imagePicker, animated: true, completion: nil)
		}
		else {
			let alertViewController = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
			viewController.present(alertViewController, animated: true, completion: nil)
		}
	}
	
	func choosePhotoFromGallery(on viewController: UIViewController) {
		if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
			imagePicker.delegate = self
			imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
			imagePicker.allowsEditing = true
			viewController.present(imagePicker, animated: true, completion: nil)
		}
		else {
			let alertViewController = UIAlertController(title: "Warning", message: "Pls open acces to gallery in settings.", preferredStyle: .alert)
			viewController.present(alertViewController, animated: true, completion: nil)
		}
	}
	
}

// MARK: - Photo selection
extension Router: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	//MARK: - Done image capture here
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		imagePicker.dismiss(animated: true, completion: nil)
		if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
			if let observer = self.observer {
				observer.selectedImage(image)
			}
		}
		self.observer = nil
	}
}
