//
//  ReusableView.swift
//  CFTProject
//
//  Created by Sukhorukov Grigory on 18.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import Foundation

protocol ReusableView: class {}
extension ReusableView {
	
	static var reuseIdentifier: String {
		get {
			return String(describing: self)
		}
	}
}

protocol NibLoadable: class {}
extension NibLoadable {
	
	static var nibName: String {
		get {
			return String(describing: self)
		}
	}
}
