//
//  UITable(Collection)View+Reusable.swift
//  CFTProject
//
//  Created by Sukhorukov Grigory on 18.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

extension UICollectionView {
	
	func register<T>(_:T.Type) where T: ReusableView, T: NibLoadable {
		self.register(UINib(nibName: T.nibName, bundle: nil),
						 forCellWithReuseIdentifier: T.reuseIdentifier)
	}
	
	func dequeueReusableCell<T: ReusableView>(_:T.Type, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
		return self.dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath)
	}
	
}

extension UITableView {
	
	func register<T>(_:T.Type) where T: ReusableView, T: NibLoadable {
		self.register(UINib(nibName: T.nibName, bundle: nil),
						 forCellReuseIdentifier: T.reuseIdentifier)
	}
	
	func dequeueReusableCell<T: ReusableView>(_:T.Type, forIndexPath indexPath: IndexPath) -> UITableViewCell {
		return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath)
	}
	
}
