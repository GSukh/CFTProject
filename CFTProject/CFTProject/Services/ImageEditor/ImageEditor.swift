//
//  ImageEditor.swift
//  CFTProject
//
//  Created by Sukhorukov Grigory on 18.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit
import CoreFoundation

enum TransformationType {
	case rotate
	case mirror
	case invertColors
}

fileprivate var rotations: [UIImageOrientation] = [
	.up,
	.right,
	.down,
	.left,
]

fileprivate var mirroredRotations: [UIImageOrientation] = [
	.upMirrored,
	.rightMirrored,
	.downMirrored,
	.leftMirrored,
]

class ImageEditor {
	static let shared = ImageEditor()
    
	func transform(image: UIImage, with type: TransformationType, async: Bool = true) -> ImageObject {
		let imageObject = ImageObject()
        let method = self.getMethod(for: type)

		switch async {
		case true:
            self.performOperation(on: imageObject, operation: { () -> UIImage? in
                method(image)
            })
		case false:
			imageObject.image = method(image)
		}
		return imageObject
	}
	
    
    // MARK: - Async methods

    // only for supporting ios 9
    // because in iOS 9 we cant use block in Thread and Timer inialization
    fileprivate struct Transformation {
        let object: ImageObject
        let operation: () -> UIImage?
        let timeInterval: Int
        let complitionDate: Date
    }
    
    fileprivate func performOperation(on object: ImageObject, operation: @escaping () -> UIImage?) {
		let timeInterval: Int = Int(arc4random() % 25) + 5
        let complitionDate = Date.init(timeIntervalSinceNow: TimeInterval(timeInterval))
        let transformation = Transformation.init(object: object, operation: operation, timeInterval: timeInterval, complitionDate: complitionDate)
       
        let thread = Thread.init(target: self, selector: #selector(threadEnter(object:)), object: transformation)
        thread.start()
    }
    
    @objc
    fileprivate func threadEnter(object: Any?) {
        guard let transformation = object as? Transformation else { return }
        
        let timer = Timer.init(timeInterval: 0.5, target: self, selector: #selector(timerTick(timer:)), userInfo: transformation, repeats: true)
        RunLoop.current.add(timer, forMode: RunLoopMode.defaultRunLoopMode)
        RunLoop.current.run()
    }
    
    @objc
    fileprivate func timerTick(timer: Timer) {
        guard let transformation = timer.userInfo as? Transformation else { return }
        
        let lifetime = -Date().timeIntervalSince(transformation.complitionDate)
        transformation.object.progress = (Float(transformation.timeInterval) - Float(lifetime)) / Float(transformation.timeInterval)
        if lifetime < 0 {
            transformation.object.image = transformation.operation()
            timer.invalidate()
            Thread.current.cancel()
        }
    }
	
    
    // MARK: - Transformations
	fileprivate func getMethod(for transformation: TransformationType) -> (UIImage) -> UIImage? {
		switch transformation {
		case .rotate: 		return rotate
		case .mirror: 		return mirror
		case .invertColors: return invertColors
		}
	}
	
	fileprivate func rotate(image: UIImage) -> UIImage? {
		guard let cgImage = image.cgImage else { return nil }
		
		var finalOrientation: UIImageOrientation?
		if let notMirroredIndex = rotations.index(of: image.imageOrientation) {
			let rotatedIndex = (notMirroredIndex + 1) % 4
			finalOrientation = rotations[rotatedIndex]
		}
		else if let mirroredIndex = mirroredRotations.index(of: image.imageOrientation) {
			let rotatedIndex = (mirroredIndex + 1) % 4
			finalOrientation = mirroredRotations[rotatedIndex]
		}
		
		let rotatedImage = UIImage.init(cgImage: cgImage, scale: 1.0, orientation: finalOrientation!)
		return renderRotated(image: rotatedImage)
	}
	
	fileprivate func mirror(image: UIImage) -> UIImage? {
		guard let cgImage = image.cgImage else { return nil }

		var finalOrientation: UIImageOrientation?
		if let notMirroredIndex = rotations.index(of: image.imageOrientation) {
			finalOrientation = mirroredRotations[notMirroredIndex]
		}
		else if let mirroredIndex = mirroredRotations.index(of: image.imageOrientation) {
			finalOrientation = rotations[mirroredIndex]
		}
		
		let rotatedImage = UIImage.init(cgImage: cgImage, scale: 1.0, orientation: finalOrientation!)
		return renderRotated(image: rotatedImage)
	}
	
	fileprivate func invertColors(image: UIImage) -> UIImage? {
		guard let cgImage = image.cgImage else { return nil }
		
		let imageRect: CGRect = CGRect.init(x: 0, y: 0, width: image.size.width, height: image.size.height)
		let colorSpace = CGColorSpaceCreateDeviceGray()
		let width = image.size.width
		let height = image.size.height
		
		let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
		guard let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
			else { return nil }
		
		context.draw(cgImage, in: imageRect)
		guard let imageRef = context.makeImage() else { return nil }
		
		
		let newImage = UIImage(cgImage: imageRef)
		
		return newImage
	}
	
	fileprivate func renderRotated(image: UIImage) -> UIImage {
		
		let imgRef: CGImage = image.cgImage!
		
		let width: CGFloat = CGFloat(imgRef.width)
		let height: CGFloat = CGFloat(imgRef.height)
		
		var transform: CGAffineTransform = .identity
		var bounds: CGRect = CGRect.init(x: 0, y: 0, width: width, height: height)
		
		
		let scaleRatio: CGFloat = 1.0
		let imageSize: CGSize = CGSize.init(width: width, height: height)
		var boundHeight: CGFloat = 0
		let orient: UIImageOrientation = image.imageOrientation
		switch(orient) {
			
		case .up: //EXIF = 1
			transform = .identity
			break;
			
		case .upMirrored: //EXIF = 2
			transform = CGAffineTransform(translationX: imageSize.width, y: 0.0)
			transform = transform.scaledBy(x: -1.0, y: 1.0)
			break;
			
		case .down: //EXIF = 3
			transform = CGAffineTransform(translationX: imageSize.width, y: imageSize.height)
			transform = transform.rotated(by: CGFloat.pi)
			break;
			
		case .downMirrored: //EXIF = 4
			transform = CGAffineTransform(translationX: 0.0, y: imageSize.height)
			transform = transform.scaledBy(x: 1.0, y: -1.0)
			break;
			
		case .leftMirrored: //EXIF = 5
			boundHeight = bounds.size.height
			bounds.size.height = bounds.size.width
			bounds.size.width = boundHeight
			transform = CGAffineTransform(translationX: imageSize.height, y: imageSize.width)
			transform = transform.scaledBy(x: -1.0, y: 1.0)
			transform = transform.rotated(by: 3.0 * CGFloat.pi / 2)
			break;
			
		case .left: //EXIF = 6
			boundHeight = bounds.size.height
			bounds.size.height = bounds.size.width
			bounds.size.width = boundHeight
			transform = CGAffineTransform(translationX: 0.0, y: imageSize.width)
			transform = transform.rotated(by: 3.0 * CGFloat.pi / 2)
			break;
			
		case .rightMirrored: //EXIF = 7
			boundHeight = bounds.size.height
			bounds.size.height = bounds.size.width
			bounds.size.width = boundHeight
			transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
			transform = transform.rotated(by: CGFloat.pi / 2)
			break;
			
		case .right: //EXIF = 8
			boundHeight = bounds.size.height
			bounds.size.height = bounds.size.width
			bounds.size.width = boundHeight
			transform = CGAffineTransform(translationX: imageSize.height, y: 0.0)
			transform = transform.rotated(by: CGFloat.pi / 2)
			break;
		}
		
		UIGraphicsBeginImageContext(bounds.size)
		
		let context: CGContext = UIGraphicsGetCurrentContext()!
		
		if (orient == .right || orient == .left) {
			context.scaleBy(x: -scaleRatio, y: scaleRatio);
			context.translateBy(x: -height, y: 0);
		}
		else {
			context.scaleBy(x: scaleRatio, y: -scaleRatio);
			context.translateBy(x: 0, y: -height);
		}
		
		context.concatenate(transform);
		
		context.draw(imgRef, in: CGRect.init(x: 0, y: 0, width: width, height: height))
		let imageCopy: UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
		UIGraphicsEndImageContext();
		
		return imageCopy;
	}
	
}
