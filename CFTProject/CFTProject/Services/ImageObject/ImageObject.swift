//
//  ImageObject.swift
//  CFTProject
//
//  Created by Sukhorukov Grigory on 18.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

@objcMembers class ImageObject: NSObject {
	dynamic var progress: Float = 0
	dynamic var image: UIImage?
	
	convenience init(image: UIImage) {
		self.init()
		self.image = image
	}
}
