//
//  AppDelegate.swift
//  CFTProject
//
//  Created by Grigoriy Sukhorukov on 17.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let photoEditorViewController = storyboard.instantiateInitialViewController()! as! PhotoEditorViewController
        let photoEditorPresenter = PhotoEditorPresenter(view: photoEditorViewController)
        photoEditorViewController.presenter = photoEditorPresenter
        
        let window = UIWindow.init()
        window.rootViewController = photoEditorViewController
        window.makeKeyAndVisible()
        
        self.window = window
        
        return true
    }

}

