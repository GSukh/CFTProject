//
//  PhotoEditorViewController.swift
//  CFTProject
//
//  Created by Grigoriy Sukhorukov on 17.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

class PhotoEditorViewController: UIViewController {
    var presenter: PhotoEditorPresenterInterface!
		
    @IBOutlet weak var buttonImage: UIButton!
    @IBOutlet weak var buttonRotate: UIButton!
    @IBOutlet weak var buttonInvert: UIButton!
    @IBOutlet weak var buttonMirror: UIButton!
    
    @IBOutlet weak var viewImage: UIImageView!
    @IBOutlet weak var viewCollection: UICollectionView!
	@IBOutlet weak var viewSegmentedContoll: UISegmentedControl!
	
	fileprivate var tapGestureRecodnizer: UIGestureRecognizer?
    
    @IBAction func buttonAction(_ sender: UIButton) {
        var maybeType: ActionType?
        if sender == buttonImage { maybeType = .onImageTap }
        else if sender == buttonRotate { maybeType = .onRotateTap }
        else if sender == buttonInvert { maybeType = .onInvertTap }
        else if sender == buttonMirror { maybeType = .onMirrorTap }
        if let type = maybeType {
            presenter.performAction(type: type)
        }
    }
	
	@IBAction func segmentedControllAction() {
		switch viewSegmentedContoll.selectedSegmentIndex {
		case 0: presenter.set(transformationStyle: .sync)
		case 1: presenter.set(transformationStyle: .async)
		default: break
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.viewCollection.delegate = self
		self.viewCollection.dataSource = self
		self.viewCollection.register(ImageCollectionViewCell.self)
		
		self.tapGestureRecodnizer = UITapGestureRecognizer.init(target: self, action: #selector(tapOnImage))
		viewImage.addGestureRecognizer(self.tapGestureRecodnizer!)
		viewImage.isUserInteractionEnabled = true
	}
	
	@objc
	func tapOnImage() {
		presenter.performAction(type: .onImageTap)
	}
	
}

extension PhotoEditorViewController: PhotoEditorViewInterface {

	func setCurrentImage(_ image: UIImage?) {
		viewImage.image = image
		buttonImage.isHidden = (image != nil)
	}

	func insertImage(at index: Int) {
		viewCollection.insertItems(at: [IndexPath(row: index, section: 0)])
	}

	func removeImage(at index: Int) {
		viewCollection.deleteItems(at: [IndexPath(row: index, section: 0)])
	}
	
	func asViewController() -> UIViewController? {
		return self
	}
}

extension PhotoEditorViewController: UICollectionViewDataSource {

	public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return presenter.getObjectsCount()
	}
	
	public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(ImageCollectionViewCell.self, forIndexPath: indexPath)
        (cell as! ImageCellInterface).set(object: nil)
		return cell
	}
}


extension PhotoEditorViewController: UICollectionViewDelegate {
	
	public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		collectionView.deselectItem(at: indexPath, animated: true)
		presenter.selectImage(at: indexPath.row)
	}
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let object = presenter.getObject(at: indexPath.row) {
            (cell as! ImageCellInterface).set(object: object)
        }
    }
}
