//
//  PhotoEditorView.swift
//  CFTProject
//
//  Created by Grigoriy Sukhorukov on 17.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

protocol PhotoEditorViewInterface: ViewInterface {
    
	func setCurrentImage(_ image: UIImage?)
	func insertImage(at index: Int)
	func removeImage(at index: Int)
	
}
