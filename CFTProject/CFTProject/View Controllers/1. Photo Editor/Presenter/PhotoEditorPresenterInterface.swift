//
//  PhotoEditorViewPresenter.swift
//  CFTProject
//
//  Created by Grigoriy Sukhorukov on 17.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

enum ActionType {
    case onImageTap
    case onRotateTap
    case onInvertTap
    case onMirrorTap
}

enum TransformationStyle {
	case sync
	case async
}

protocol PhotoEditorPresenterInterface {
    
    func performAction(type: ActionType)
	func selectImage(at index: Int)
	
	func set(transformationStyle: TransformationStyle)
	
	func getObjectsCount() -> Int
	func getObject(at index: Int) -> ImageObject?
	
}
