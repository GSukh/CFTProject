//
//  PhotoEditorViewModel.swift
//  CFTProject
//
//  Created by Grigoriy Sukhorukov on 17.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

class PhotoEditorPresenter: PhotoEditorPresenterInterface {
    
    unowned let view: PhotoEditorViewInterface
	
	fileprivate var currentImage: UIImage? {
		didSet {
			self.view.setCurrentImage(currentImage)
		}
	}
	
	fileprivate var images: [ImageObject] = []
	fileprivate var asyncTransformationEnabled: Bool = false
	
    init(view: PhotoEditorViewInterface) {
        self.view = view
    }
    
    func performAction(type: ActionType) {
		switch type {
		case .onImageTap:
			Router.shared.rout(.choosePhoto, on: (view: self.view, observer: self))
		case .onRotateTap:
			self.rotateCurrentImage()
		case .onInvertTap:
			self.invertColorsOfCurrentImage()
		case .onMirrorTap:
			self.mirrorCurrentImage()
		}
    }
	
	func set(transformationStyle: TransformationStyle) {
		switch transformationStyle {
		case .sync: 	self.asyncTransformationEnabled = false
		case .async: 	self.asyncTransformationEnabled = true
		}
	}
	
	func selectImage(at index: Int) {
		self.showActionsForImage(at: index)
	}
	
	func getObjectsCount() -> Int {
		return images.count
	}
	
	func getObject(at index: Int) -> ImageObject? {
		return images[index]
	}
	
}

extension PhotoEditorPresenter: RouterObserver {
	func selectedImage(_ image: UIImage) {
		self.currentImage = image
	}
}

fileprivate extension PhotoEditorPresenter {
	
	func append(object: ImageObject) {
		self.images.insert(object, at: 0)
		self.view.insertImage(at: 0)
	}
	
	func rotateCurrentImage() {
		guard let image = currentImage else { return }
		let object = ImageEditor.shared.transform(image: image, with: .rotate, async: asyncTransformationEnabled)
		append(object: object)
	}
	
	func invertColorsOfCurrentImage() {
		guard let image = currentImage else { return }
		let object = ImageEditor.shared.transform(image: image, with: .invertColors, async: asyncTransformationEnabled)
		append(object: object)
	}
	
	func mirrorCurrentImage() {
		guard let image = currentImage else { return }
		let object = ImageEditor.shared.transform(image: image, with: .mirror, async: asyncTransformationEnabled)
		append(object: object)
	}
	
	func showActionsForImage(at index: Int) {
		
		let alertText = AlertText.init(title: "", message: "What you want to do with this image?")
		let saveAction = AlertAction.init(title: "Save", style: .defauld) { [weak self] in
			self?.saveImage(at: index)
		}
		let editAction = AlertAction.init(title: "Edit", style: .defauld) { [weak self] in
			self?.currentImage = self?.images[index].image
		}
		let deleteAction = AlertAction.init(title: "Delete", style: .delete) { [weak self] in
			self?.deleteImage(at: index)
		}
		let routing = Routing.actionSheet(alertText, [saveAction, editAction, deleteAction], {})
		Router.shared.rout(routing, on: (view: self.view, observer: nil))
	}
	
	func saveImage(at index: Int) {
		guard let image = images[index].image else { return }
		UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
	}
	
	func deleteImage(at index: Int) {
		images.remove(at: index)
		view.removeImage(at: index)
	}
	
}
