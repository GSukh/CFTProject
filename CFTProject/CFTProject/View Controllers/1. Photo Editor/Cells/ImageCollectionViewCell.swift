//
//  ImageCollectionViewCell.swift
//  CFTProject
//
//  Created by Sukhorukov Grigory on 18.11.17.
//  Copyright © 2017 Grigoriy Sukhorukov. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell, ReusableView, NibLoadable {

	@IBOutlet weak var viewImage: UIImageView!
	@IBOutlet weak var viewProgressTitle: UILabel!
	@IBOutlet weak var viewProgress: UIProgressView!
	
	fileprivate var progressObserver: NSKeyValueObservation?
	fileprivate var imageObserver: NSKeyValueObservation?

	override func awakeFromNib() {
        super.awakeFromNib()
    }

}

extension ImageCollectionViewCell: ImageCellInterface {
	func set(object: ImageObject?) {
		
		guard object != nil else {
            self.progressObserver?.invalidate()
            self.imageObserver?.invalidate()
            
            self.viewImage.image = nil
            self.viewProgress.progress = 0
            self.viewProgress.isHidden = false
            self.viewProgressTitle.text = "0% complete"
            self.viewProgressTitle.isHidden = false
			return
		}
		
		if let image = object?.image {
			self.set(image: image)
		}
		
		self.progressObserver?.invalidate()
		self.progressObserver = object!.observe(\.progress) { [weak self] (object, _) in
			DispatchQueue.main.async {
				self?.set(progress: object.progress)
			}
		}
		
		self.imageObserver?.invalidate()
		self.imageObserver = object!.observe(\.image) { [weak self] (object, _) in
			guard let image = object.image else { return }
			DispatchQueue.main.async {
				self?.set(image: image)
			}
		}
	}
	
	fileprivate func set(image: UIImage) {
		self.viewImage.image = image
		self.viewProgress.isHidden = true
		self.viewProgressTitle.isHidden = true
	}
	
	fileprivate func set(progress: Float) {
		self.viewProgress.progress = progress
		let percent = Int(progress * 100)
		self.viewProgressTitle.text = "\(percent)% complete"
	}
}
